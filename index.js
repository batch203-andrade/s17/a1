/*
    1. Create a function which is able to prompt the user to provide their fullname, age, and location.
        -use prompt() and store the returned value into a function scoped variable within the function.
        -display the user's inputs in messages in the console.
        -invoke the function to display your information in the console.
        -follow the naming conventions for functions.
*/

//first function here:

let fullName = prompt('Enter Full Name: ');
let age = prompt('Enter Age: ');
let locate = prompt('Enter Location: ');

function displayDetails(name, age, loc) {
    console.log(`Hello, ${name}`);
    console.log(`You are ${age} years old.`);
    console.log(`You live in ${loc}.`);
}

displayDetails(fullName, age, locate);



/*
    2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
        -invoke the function to display your information in the console.
        -follow the naming conventions for functions.
	
*/

//second function here:
const bands = ['The Beatles', 'Metallica', 'The Eagles', 'L\'arc~en~Ciel', 'Eraserheads'];
function favoriteBandsArtist(bands) {
    return bands.map((e, i) => console.log(`${i + 1}. ${e}`));
}

favoriteBandsArtist(bands);

/*
    3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
        -Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        -invoke the function to display your information in the console.
        -follow the naming conventions for functions.

*/

//third function here:

const movieFavorites = [
    ['The Godfather', '97%'],
    ['The Godfather, Part II', '96%'],
    ['Shawshank Redemption', '91%'],
    ['To Kill A Mockingbird', '93%'],
    ['Psycho', '96%'],
];

function topFiveMovies(favorites) {
    return favorites.map((e, i) => {
        console.log(`${i + 1}. ${e[0]}`);
        console.log(`Rotten Tomatoes Rating: ${e[1]}`);
    });
}

topFiveMovies(movieFavorites);

/*
    4. Debugging Practice - Debug the following codes and functions to avoid errors.
        -check the variable names
        -check the variable scope
        -check function invocation/declaration
        -comment out unusable codes.
*/

let printUsers = function () {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");
    
    console.log("You are friends with:")
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};
printUsers();


// console.log(friend1);
// console.log(friend2);